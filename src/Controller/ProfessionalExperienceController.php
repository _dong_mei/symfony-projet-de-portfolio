<?php

namespace App\Controller;

use App\Entity\ProfessionalExperience;
use App\Form\ProfessionalExperienceType;
use App\Repository\ProfessionalExperienceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/professionalExperience')]
class ProfessionalExperienceController extends AbstractController
{
    #[Route('/', name: 'app_professional_experience_index', methods: ['GET'])]
    public function index(ProfessionalExperienceRepository $professionalExperienceRepository): Response
    {
        return $this->render('professional_experience/index.html.twig', [
            'professional_experiences' => $professionalExperienceRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_professional_experience_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ProfessionalExperienceRepository $professionalExperienceRepository): Response
    {
        $professionalExperience = new ProfessionalExperience();
        $form = $this->createForm(ProfessionalExperienceType::class, $professionalExperience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $professionalExperienceRepository->add($professionalExperience);
            return $this->redirectToRoute('app_professional_experience_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('professional_experience/new.html.twig', [
            'professional_experience' => $professionalExperience,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_professional_experience_show', methods: ['GET'])]
    public function show(ProfessionalExperience $professionalExperience): Response
    {
        return $this->render('professional_experience/show.html.twig', [
            'professional_experience' => $professionalExperience,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_professional_experience_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ProfessionalExperience $professionalExperience, ProfessionalExperienceRepository $professionalExperienceRepository): Response
    {
        $form = $this->createForm(ProfessionalExperienceType::class, $professionalExperience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $professionalExperienceRepository->add($professionalExperience);
            return $this->redirectToRoute('app_professional_experience_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('professional_experience/edit.html.twig', [
            'professional_experience' => $professionalExperience,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_professional_experience_delete', methods: ['POST'])]
    public function delete(Request $request, ProfessionalExperience $professionalExperience, ProfessionalExperienceRepository $professionalExperienceRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$professionalExperience->getId(), $request->request->get('_token'))) {
            $professionalExperienceRepository->remove($professionalExperience);
        }

        return $this->redirectToRoute('app_professional_experience_index', [], Response::HTTP_SEE_OTHER);
    }
}
