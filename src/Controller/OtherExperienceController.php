<?php

namespace App\Controller;

use App\Entity\OtherExperience;
use App\Form\OtherExperienceType;
use App\Repository\OtherExperienceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/otherExperience')]
class OtherExperienceController extends AbstractController
{
    #[Route('/', name: 'app_other_experience_index', methods: ['GET'])]
    public function index(OtherExperienceRepository $otherExperienceRepository): Response
    {
        return $this->render('other_experience/index.html.twig', [
            'other_experiences' => $otherExperienceRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_other_experience_new', methods: ['GET', 'POST'])]
    public function new(Request $request, OtherExperienceRepository $otherExperienceRepository): Response
    {
        $otherExperience = new OtherExperience();
        $form = $this->createForm(OtherExperienceType::class, $otherExperience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $otherExperienceRepository->add($otherExperience);
            return $this->redirectToRoute('app_other_experience_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('other_experience/new.html.twig', [
            'other_experience' => $otherExperience,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_other_experience_show', methods: ['GET'])]
    public function show(OtherExperience $otherExperience): Response
    {
        return $this->render('other_experience/show.html.twig', [
            'other_experience' => $otherExperience,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_other_experience_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, OtherExperience $otherExperience, OtherExperienceRepository $otherExperienceRepository): Response
    {
        $form = $this->createForm(OtherExperienceType::class, $otherExperience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $otherExperienceRepository->add($otherExperience);
            return $this->redirectToRoute('app_other_experience_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('other_experience/edit.html.twig', [
            'other_experience' => $otherExperience,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_other_experience_delete', methods: ['POST'])]
    public function delete(Request $request, OtherExperience $otherExperience, OtherExperienceRepository $otherExperienceRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$otherExperience->getId(), $request->request->get('_token'))) {
            $otherExperienceRepository->remove($otherExperience);
        }

        return $this->redirectToRoute('app_other_experience_index', [], Response::HTTP_SEE_OTHER);
    }
}
