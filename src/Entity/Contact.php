<?php

namespace App\Entity;

use App\Repository\ContactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ContactRepository::class)]
class Contact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $adress;

    #[ORM\Column(type: 'string', length: 255)]
    private $town;

    #[ORM\Column(type: 'string', length: 255)]
    private $postalCode;

    #[ORM\Column(type: 'string', length: 255)]
    private $country;

    #[ORM\OneToMany(mappedBy: 'contact', targetEntity: Link::class)]
    private $linkToSocialNetWork;

    public function __construct()
    {
        $this->linkToSocialNetWork = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection<int, Link>
     */
    public function getLinkToSocialNetWork(): Collection
    {
        return $this->linkToSocialNetWork;
    }

    public function addLinkToSocialNetWork(Link $linkToSocialNetWork): self
    {
        if (!$this->linkToSocialNetWork->contains($linkToSocialNetWork)) {
            $this->linkToSocialNetWork[] = $linkToSocialNetWork;
            $linkToSocialNetWork->setContact($this);
        }

        return $this;
    }

    public function removeLinkToSocialNetWork(Link $linkToSocialNetWork): self
    {
        if ($this->linkToSocialNetWork->removeElement($linkToSocialNetWork)) {
            // set the owning side to null (unless already changed)
            if ($linkToSocialNetWork->getContact() === $this) {
                $linkToSocialNetWork->setContact(null);
            }
        }

        return $this;
    }
}
